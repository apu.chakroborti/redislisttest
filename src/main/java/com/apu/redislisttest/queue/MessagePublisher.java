package com.apu.redislisttest.queue;

public interface MessagePublisher {

    void publish(final String message);
}
